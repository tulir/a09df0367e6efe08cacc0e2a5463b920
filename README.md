Dependencies: bash, rm, mv, cp, cat, tar, curl, jq

To update riot to the latest version, simply run the script. Optionally pass a riot-web tag to download a specific release (useful for release candidates)

The first `cd` in the script is where it runs. Riot is installed to path/riot (so `/var/www/riot` by default). The script also uses `riot.bak` and `riot.new` directories and downloads the archive to `riot-tmp.tar.gz`.

Support/bug reports/feature requests/etc: [#maunium:maunium.net](https://matrix.to/#/#maunium:maunium.net)